# topos

Dépôt pour la mise en place d'outils permettant d'analyser la topologie textuelle. Projet porté par Dominique Legallois et Antoine Silvestre de Sacy.

## Présentation et enjeux

La topologie textuelle désigne les lieux d'apparition d'une unité lexicale (ou autre modélisation) dans un texte. L'objectif est de s'intéresser à ces unités et à leur distribution topologique au sein d'un texte. Trois cas de figure semblent pouvoir être identifiés :

- soit la distribution est homogène (donc pas vraiment d'intérêt)
- soit la distribution paraît aléatoire (peut-être pas intéressant non plus)
- soit la distribution montre qu'un mot X concentre ses emplois dans une zone précise du texte, ou dans plusieurs zones (par exemple, le mot X va apparaître en positions 203, 220, 230 (première zone) et (509, 515, 530) (2e zone).

L'objectif est donc d'identifier ces passages narratifs où semblent apparaître ce que l'on pourrait nommer des __rafales lexicales__ (Pierre Lafon) : 

> "L'identification d'une série de termes répétés en même temps de manière importante dans une courte séquence de texte (i.e des "rafales") permet de mettre en évidence les "paquets de sens" qui constituent des épisodes. En suivant ces rafales de termes, on peut espérer repérer les ruptures thématiques (fin d'une série de rafales), les passages où les fils du récit se dénouent cependant qu'un nouveau thème (ou épisode ici) est développé (nouvelle série de rafales)."

> "Les relevés de fréquence négligent le fait que les occurrences d'un texte constituent une suite ordonnée. Afin de prendre en considération le caractère ordonné d'un texte et de saisir un autre aspect du fonctionnement de ses éléments, on remplace ici le point de vue fréquentiel, habituel, par un point de vue séquentiel."

## Fonctionnalités

- Annotation d'une oeuvre (lemmatisation, pos-tagging, morphologie, etc.) avec l'annotateur UDPipe.
- Calcul des fréquences (absolues, relatives) à l'échelle de l'oeuvre.
- Découpage de l'oeuvre à l'étude en fenêtres d'analyse de x mots ou unités.
- Calcul des fréquences relatives de ces unités au sein des fenêtres.
- Analyse des rapport entre ces fréquences à l'échelle de la fenêtre et à l'échelle de l'oeuvre afin de repérer des sur-représentations au sein de certaines fenêtres textuelles.
- Visualisations.
- Même méthodologie en implémentant un calcul du tf-idf à l'échelle de l'oeuvre et des fenêtres.
- Visualisations et fonctionnalité de retours aux fenêtres textuelles comprenant des unités sur-représentées.

## Références

- Lafon Pierre. *Statistiques des localisations des formes d'un texte*. In: *Mots*, n°2, mars 1981. *Qu'est-ce que le vocabulaire spécifique d'un texte politique?* pp. 157-188. DOI : https://doi.org/10.3406/mots.1981.1026.
- Mathieu Brugidou, Pierre Le Quéau. *Les « rafales », une méthode pour identifier les différents épisodes d'un récit : contribution au traitement et à l'interprétation des entretiens non directifs de recherche*. *Bulletin de Méthodologie Sociologique / Bulletin of Sociological Methodology*, 1999, octobre (64), pp.47-62. (https://shs.hal.science/halshs-00493342).
